-module(cache_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_, _) ->
    cache_sup:start_link().

stop(_) ->
    ok.
