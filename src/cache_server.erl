-module(cache_server).

-behaviour(gen_server).

-export([stop/1, start_link/1, start_link/2, start/1, start/2]).
-export([new/1, insert/4, lookup/2, lookup_by_date/3]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2]).

-include("cache_tables.hrl").

-define(GC_INTERVAL, 3600).

start(Name) ->
    start(Name, [{drop_interval, ?GC_INTERVAL}]).

start(Name, Options) ->
    gen_server:start({local, Name}, ?MODULE, Options, []).

start_link(Name) ->
    start_link(Name, [{drop_interval, ?GC_INTERVAL}]).

start_link(Name, Options) ->
    gen_server:start_link({local, Name}, ?MODULE, Options, []).

stop(Name) ->
    gen_server:call(Name, stop).

% gen_server
init(Options) ->
    Interval = proplists:get_value(drop_interval, Options, ?GC_INTERVAL),
    IntervalMs = erlang:convert_time_unit(Interval, second, millisecond),
    timer:send_interval(IntervalMs, gc),
    {ok, cache_crud:new()}.

handle_call({insert, Key, Val, TimeSec}, _, Tabs) ->
    {reply, cache_crud:insert(Tabs, Key, Val, TimeSec), Tabs};

handle_call({lookup, Key}, _, Tabs)->
    {reply, cache_crud:lookup(Tabs, Key), Tabs};

handle_call({lookup_by_date, DateFrom, DateTo}, _, Tabs) ->
    {reply, cache_crud:lookup_by_date(Tabs, DateFrom, DateTo), Tabs};

handle_call(stop, _From, State) ->
    {stop, normal, stopped, State};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(gc, Tabs) ->
    cache_crud:delete_obsolete(Tabs),
    {noreply, Tabs};

handle_info(_Info, State) ->
    {noreply, State}.

%% 'client' functions
new(TableName) ->
    {ok, _} = start_link(TableName),
    ok.

insert(TableName, Key, Val, TtlSec) ->
    gen_server:call(TableName, {insert, Key, Val, TtlSec}).

lookup(TableName, Key) ->
    gen_server:call(TableName, {lookup, Key}).

lookup_by_date(TableName, DateFrom, DateTo) ->
    gen_server:call(TableName, {lookup_by_date, DateFrom, DateTo}).
