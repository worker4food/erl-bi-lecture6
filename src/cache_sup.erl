-module(cache_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start_child/1, start_child/2]).
-export([init/1]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_child(Name) ->
    supervisor:start_child(?MODULE, [Name]).

start_child(Name, Opts) ->
    supervisor:start_child(?MODULE, [Name, Opts]).

init(_) ->
    Spec = [#{
        id       => cache_server,
        start    => {cache_server, start_link, []},
        restart  => permanent,
        shutdown => brutal_kill,
        type     => worker
    }],
    {ok, {{simple_one_for_one, 10, 60}, Spec}}.
