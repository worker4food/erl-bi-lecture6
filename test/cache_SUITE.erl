-module(cache_SUITE).

-include_lib("common_test/include/ct.hrl").

-compile(export_all).

-define(Sz, 1000).

groups() -> [
    {cache_crud, [parallel], [
        insert,
        lookup,
        lookup_expired,
        lookup_by_date,
        lookup_by_date_expired]},
    {cache_server, [shuffle], [
        server_insert,
        server_lookup,
        server_lookup_expired,
        server_lookup_by_date,
        server_lookup_by_date_expired,
        server_gc]}
].

all() -> [
    {group, cache_crud},
    {group, cache_server}
].

init_per_suite(Cfg) ->
    application:start(lecture6),
    Cfg.

end_per_suite(_) ->
    application:stop(lecture6),
    ok.

init_per_group(cache_server, Cfg) ->
    [{starts_server, yes} | Cfg];
init_per_group(_, Cfg) ->
    Cfg.

end_per_group(_, Cfg) ->
    proplists:delete(starts_server, Cfg).

init_per_testcase(_, Cfg) ->
    Cfg.

end_per_testcase(_, Cfg) ->
    case proplists:get_value(starts_server, Cfg) of
        yes ->
            Pid = whereis(test),
            ok = supervisor:terminate_child(cache_sup, Pid);
        undefined ->
            ok
    end.

insert(_) ->
    T = cache_crud:new(),
    [ok = cache_crud:insert(T, X, X, X) || X <- lists:seq(1, ?Sz)].

lookup(_) ->
    T = cache_crud:new(),
    Keys = lists:seq(1, ?Sz),
    [cache_crud:insert(T, X, X, 10000) || X <- Keys],
    [{ok, X} = cache_crud:lookup(T, X) || X <- Keys].

lookup_expired(_) ->
    T = cache_crud:new(),
    Keys = lists:seq(1, ?Sz),
    [cache_crud:insert(T, X, X, -1) || X <- Keys],
    [undefined = cache_crud:lookup(T, X) || X <- Keys].

lookup_by_date(_) ->
    T = cache_crud:new(),
    Keys = lists:seq(1, ?Sz),
    Start = calendar:universal_time(),
    [cache_crud:insert(T, X, X, 10000) || X <- Keys],
    End = calendar:universal_time(),
    {ok, Vals} = cache_crud:lookup_by_date(T, Start, End),
    Keys = lists:sort(Vals).

lookup_by_date_expired(_) ->
    T = cache_crud:new(),
    Keys = lists:seq(1, ?Sz),
    Start = calendar:universal_time(),
    [cache_crud:insert(T, X, X, -1) || X <- Keys],
    End = calendar:universal_time(),
    {ok, []} = cache_crud:lookup_by_date(T, Start, End).

server_insert(_) ->
    {ok, _} = cache_sup:start_child(test),
    [ok = cache_server:insert(test, X, X, X) || X <- lists:seq(1, ?Sz)].

server_lookup(_) ->
    {ok, _} = cache_sup:start_child(test),
    Keys = lists:seq(1, ?Sz),
    [cache_server:insert(test, X, X, 10000) || X <- Keys],
    [{ok, X} = cache_server:lookup(test, X) || X <- Keys].

server_lookup_expired(_) ->
    {ok, _} = cache_sup:start_child(test),
    Keys = lists:seq(1, ?Sz),
    [cache_server:insert(test, X, X, -1) || X <- Keys],
    [undefined = cache_server:lookup(test, X) || X <- Keys].

server_lookup_by_date(_) ->
    {ok, _} = cache_sup:start_child(test),
    Keys = lists:seq(1, ?Sz),
    Start = calendar:universal_time(),
    [cache_server:insert(test, X, X, 10000) || X <- Keys],
    End = calendar:universal_time(),
    {ok, Vals} = cache_server:lookup_by_date(test, Start, End),
    Keys = lists:sort(Vals).

server_lookup_by_date_expired(_) ->
    cache_sup:start_child(test),
    Keys = lists:seq(1, ?Sz),
    Start = calendar:universal_time(),
    [cache_server:insert(test, X, X, -1) || X <- Keys],
    End = calendar:universal_time(),
    {ok, []} = cache_server:lookup_by_date(test, Start, End).

server_gc(_) ->
    {ok, _} = cache_sup:start_child(test, [{drop_interval, 1}]),
    Keys = lists:seq(1, ?Sz),
    [ok = cache_server:insert(test, X, X, 2) || X <- Keys],
    [{ok, X} = cache_server:lookup(test, X) || X <- Keys],
    ct:sleep({seconds, 5}), % ensure gc started and hope its completed.
    [undefined = cache_server:lookup(test, X) || X <- Keys].
